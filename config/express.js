'use strict';
let express = require('express');
let bodyParser = require('body-parser');
let methodOverride = require('method-override');
let path = require('path');
let config = require('../config/app.config');
 
let app = express();
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//configure middlewares
require('../app/routes')(app);
//app.use('/api', require('../app/routes')(app));
app.listen(config.PORT, function () {
	console.log("UPI Services running on port " + "" + config.PORT);
});
 
 
 
