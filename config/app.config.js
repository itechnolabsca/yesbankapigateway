module.exports = {
	app: {
		title: 'UPI SERVICES',
		description: 'UP SERVICES API...'		
	},
	API: {
		CHECK_VIRTUAL_ADDRESS: "https://uatsky.yesbank.in/app/uat/upi/CheckVirtualAddress",
		ME_REFUND_SERVER_REQUEST: "https://uatsky.yesbank.in/app/uat/upi/meRefundServerReq",
		TRANSACTION_STATUS_QUERY: "https://uatsky.yesbank.in/app/uat/upi/transactionStatusQuery",
		ME_TRANS_COLLECT_SVC: "https://uatsky.yesbank.in/app/uat/upi/meTransCollectSvc",
		ME_PAY_SERVER_REQUEST	: "https://uatsky.yesbank.in/app/uat/upi/mePayServerReq"
	},
    MYSQL: {
        SAVE_COLLECT_REQUEST: "http://34.224.133.185:3002/saveCollectRequest",
        UPDATE_COLLECT_RESPONSE: "http://34.224.133.185:3002/updateCollectResponse",
    },
    requestMsg: "1AD4F41171F0EA0C24A91CA5FA5807CD8704BAF9BA2DBBBF681F221240E92CF1A2DA364516E964C2221C54AF2E703BF06131916DB00CA032FD74B15F902995F07EF7B1FDDD6DADBEA205846926D7DAFBB6996821DAAF01122932F255CC70DB4B8FEBFCD188F7316B7997A4B3695BB45C041228D458BFE7AF6014168A93278BB3",
    pgMerchantId: "YES0000000000427",
    SECRET_ID: "C2iR7mD1yS7vI8dO4dS0vB4sC4dA2xW4oB8pC2jN1nE1nG5wT0",
	CLIENT_ID: "1c651658-d561-483c-9ec6-1d22e8bf88ab",
	PORT: process.env.PORT || 3009
	 
	 

};