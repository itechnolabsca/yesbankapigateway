'use strict';
let crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';
 let config = require('../../config/app.config');
 var axios = require('axios');
/*********************ENCRYPT MESSAGE */



exports.encrypt =  function(data, callback) {
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
        axios({
            method: 'post',
            headers: {
                'content-type': 'applicaton/json',
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID
            },
            url: "https://uatsky.yesbank.in/app/uat/upi/getEncrypt",
            data: {"requestMsg": data, "pgMerchantId": config.pgMerchantId}
        }).then(function (response) {
            callback(null, response.data)
        }, function (response) {
            callback(null, response)
            console.log("**************ERROR*********", response)
        });
}
/********************DECRYPT MESSAGE WITH KEY */
exports.decrypt= function (data, callback){
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    var t = axios({
        method: 'post',
        headers: {
            'content-type': 'applicaton/json',
            'X-IBM-Client-ID': config.CLIENT_ID,
            'X-IBM-Client-Secret': config.SECRET_ID
        },
        url: "https://uatsky.yesbank.in/app/uat/upi/getDecrypt",
        data: {"requestMsg":data, "pgMerchantId":config.pgMerchantId}
    }).then(function (response) {
        callback(null, response.data)
    }, function (response) {
        callback(null, response)
        console.log("**************ERROR*********", response)
    });
}
 
