'use strict';
let upiSecurity = require('../utils/upiSecurity');
let transIdLength = 11;
let config = require('../../config/app.config');
let request = require('request-promise');
var axios = require('axios');
var async = require('async');




let agentOptions;
let agent;
let fs= require("fs");
let https = require("https");
let Boom = require("boom");
let reqMod = require("request");
let defError = Boom.create(400,"OPERATOR_NOT_FOUND");
let sslKey = fs.readFileSync(__dirname+'/Renwed_UATSKYorg.crt','utf8');
agentOptions = {
    host: "sky.yesbank.in",
    port: '443',
    path: '/',
    rejectUnauthorized: false,
    cert: sslKey
};
agent = new https.Agent(agentOptions);



module.exports = function (app) {
    app.route('/api/encrypt').post(encryptData);
    app.route('/api/mePayServerReq').post(mePayServerRequest);
    app.route('/api/meTransCollectSvc').post(meTransCollectSvc);
    app.route('/api/transactionStatusQuery').post(transactionStatusQuery);
    app.route('/api/meRefundServerReq').post(meRefundServerReq);
    app.route('/api/CheckVirtualAddress').post(checkVirtualAddress);
    app.route('/api/CheckVirtualAddressNew').post(checkVirtualAddress1);
}
/******************CHECK VIRTUAL ADDRESS**********/

function getRandomVal() {
    let transId = require("secure-random")(transIdLength, {type: 'Buffer'}).toString('hex');
    var date = new Date().getDate()
    var month = new Date().getMonth()
    var year = new Date().getFullYear()
    date = ('0' + date).slice(-2)
    month = ('0' + month).slice(-2)
    return date + "" + Number(month + 1) + "" + year + "" + transId;
}

function checkVirtualAddress1(req, res, next) {
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    var val = config.pgMerchantId + "|" + getRandomVal() + "|" + req.body.VirtualAddress + "|T|yapapp|" + req.body.location + "|" + req.body.address +
        "|172.16.50.168|MOB|5200000200010004000639292929292|Android5.1.1|862315036516077|862315036516077|862315036516077|862315036516077|862315036516077|NA|NA|NA|NA|NA|NA|NA|NA|NA|NA|"
    upiSecurity.encrypt(val, "P2P",function (err, data) {
        var t = axios({
            method: 'post',
            headers: {
                'content-type': 'applicaton/json',
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID
            },
            url: config.API.CHECK_VIRTUAL_ADDRESS,
            data: {"requestMsg": data, "pgMerchantId": config.pgMerchantId},
            /*agent: agent*/
        }).then(function (response) {
            upiSecurity.decrypt(response.data, "P2P", function (err, data) {
                res.status(200).send(data.split("|"))
            })
        }, function (response) {
            res.status(400).send(response.response.data)
        })
    })
}


function checkVirtualAddress(req, res, next) {
    // process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    //R Registration
    //T Transaction
    var val = config.pgMerchantId + "|" + getRandomVal() + "|" + req.body.VirtualAddress + "|T|cu app|0.0 ,0.0 |Mumbai|172.16.50.65|MOB|5200000200010004000639292929292|Android7.0|351928089548108|8991000900974951819f|545d4a3fa251e3e3|02:00:00:00:00:00|02:00:00:00:00:00|||||||||NA|NA"
    upiSecurity.encrypt(val, "P2P", function (err, data) {
        var t = request({
            method: 'post',
            headers: {
                //  'content-type': 'applicaton/json',
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID
            },
            url: config.API.CHECK_VIRTUAL_ADDRESS,
            body: JSON.stringify({"requestMsg": data.toString(), "pgMerchantId": config.pgMerchantId}),
            /*agent: agent*/
        }).then(function (response) {
            upiSecurity.decrypt(response, "P2P",function (err, data) {
                var finalData = data.split("|")
                if (finalData[2] == "NA") {
                    res.status(400).send({"statusCode": 400, "data": {}, "message": "NOT FOUND"})
                }
                if (finalData[3] == "F") {
                    res.status(400).send({"statusCode": 400, "data": {}, "message": finalData[4]})
                }
                else {
                    res.status(200).send({"statusCode": 200, "message": "", "data": finalData[2]})
                }
            })
        }, function (response) {
            console.log("error", response)
            res.status(400).send({"statusCode": 400, "message": response.response.data, "data": ""})
        })
    })
}

/*********************REFUND SERVER REQUEST**** */
function meRefundServerReq(req, res, next) {
    console.log('here')
    var reqObj = {
        "mid": config.pgMerchantId,
        "hdnOrderID": getRandomVal(),
        "OrgOrderNo": req.body.OrderId,
        "OrgTrnRefNo": "",
        "OrgCustRefNo": "",
        "remark": req.body.remark,
        "amount": req.body.amount,
        "currCode": "INR",
        "paymentType": req.body.p2Type,
        "txnType": "PAY",
        "addField1": "",
        "addField2": "",
        "addField3": "",
        "addField4": "",
        "addField5": "",
        "addField6": "",
        "addField7": "",
        "addField8": "",
        "addField9": "NA",
        "addField10": "NA"
    };
    var yesbankReqArray = [], yesbankRequestString = "";

    for (var k in reqObj) {
        if (Object.prototype.hasOwnProperty.call(reqObj, k)) {
            yesbankReqArray.push(reqObj[k]);
        }
    }
    yesbankRequestString = yesbankReqArray.join("|")
    console.log(yesbankRequestString, '==============')
    upiSecurity.encrypt(yesbankRequestString, "P2M", function (err, data) {
        console.log(err, data, '===========', config.API.ME_REFUND_SERVER_REQUEST)
        var t = request({
            method: 'post',
            headers: {
                //  'content-type': 'applicaton/json',
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID
            },
            url: config.API.ME_REFUND_SERVER_REQUEST,
            body: JSON.stringify({"requestMsg": data.toString(), "pgMerchantId": config.pgMerchantId}),
            agent:agent
        }).then(function (response) {
            upiSecurity.decrypt(response, "P2M", function (err, data) {
                var finalData = data.split("|")
                console.log(finalData, '===================')
                if (finalData[2] == "NA") {
                    res.status(400).send({"statusCode": 400, "data": {}, "message": "NOT FOUND"})
                }
                if (finalData[3] == "F") {
                    res.status(400).send({"statusCode": 400, "data": {}, "message": finalData[4]})
                }
                else {
                    res.status(200).send({"statusCode": 200, "message": "", "data": finalData[2]})
                }
            })
        }, function (response) {
            console.log("error", response)
            res.status(400).send({"statusCode": 400, "message": response.response.data, "data": ""})
        })
    })
}


/*************RETURNS TRANSACTION STATUS QUERY***** */
function transactionStatusQuery(req, res, next) {
    ////////////////////GETTING COLLECT API RESPONSE/////////////////////////////////////
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    var val = config.pgMerchantId + "|" + req.body.MeRefNo + "||||||||||||NA|NA"
    upiSecurity.encrypt(val, "P2M", function (err, data) {
        var t = axios({
            method: 'post',
            headers: {
                'content-type': 'applicaton/json',
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID
            },
            url: config.API.TRANSACTION_STATUS_QUERY,
            data: {"requestMsg": data, "pgMerchantId": config.p2mMerchantId}
        }).then(function (response) {
            upiSecurity.decrypt(response.data, "P2M", function (err, data) {
                console.log(err, data)
                var resp = data.split("|")
                if (data.length > 0) {
                    res.status(200).send({"statusCode": 200, "data": resp, "message": "SUCCESS"})
                }
                else {
                    res.status(400).send({"statusCode": 400, "data": {}, "message": "ERROR IN SAVE_COLLECT_REQUEST"})
                }

            })
        }, function (response) {
            res.status(400).send(response.response.data)
        })
    })
}
/*****************TRANSACTION COLLECTION SVC*************/
function meTransCollectSvc(req, res, next) {



    var resp = {}
    var reqObj = {
        "mid": config.p2mMerchantId,
        "hdnOrderID": getRandomVal(),
        "payerVirtualAdd": req.body.VirtualAddress,
        "trnAmt": req.body.amount,
        "trnRemarks": "CU COLLECT",
        "expiryType": "EXPAFTER",
        "expiryValue": "100",
        "mcccode": "5411",
        "payerAccNo": "",
        "payerIFSC": "",
        "payerMobNo": "",
        "payerMMID": "",
        "payerAadharNo": "",
        "payeeVirtualAdd": "",
        "payeeAccNo": "",
        "payeeIFSC": "",
        "payeeAadharNo": "",
        "payeeMobNo": "",
        "payeeMmid": "",
        "addField1": "",
        "addField2": "",
        "addField3": "",
        "addField4": "",
        "addField5": "",
        "addField6": "",
        "addField7": "",
        "addField8": "",
        "addField9": "NA",
        "addField10": "NA"
    };
    var yesbankReqArray = [], yesbankRequestString = "";
    for (var k in reqObj) {
        if (Object.prototype.hasOwnProperty.call(reqObj, k)) {
            yesbankReqArray.push(reqObj[k]);
        }
    }
    yesbankRequestString = yesbankReqArray.join("|")
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
    upiSecurity.encryptP2M(yesbankRequestString, function (err, data) {
        console.log(yesbankRequestString)
        var options = {
            method: 'POST',
            url: config.API.ME_TRANS_COLLECT_SVC,
            headers: {
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID,
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                requestMsg: data,
                pgMerchantId: config.p2mMerchantId
            })
        };

        request(options, function (error, response, body) {
            if (error) {
                res.status(400).send({"statusCode": 400, "data": err})
            }
            else {
                upiSecurity.decryptP2M(body, function (err, data) {
                    console.log(err, data)
                    resp = data.split("|")
                    res.status(200).send({"statusCode": 200, "data": resp})
                })
            }
        });
    })
}

/**************PAY SERVER REQUEST******** */
function mePayServerRequest(req, res, next) {

    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

    var reqObj = {
        "mid" : config.p2mSpecialMerchantId,
        "hdnOrderID" : getRandomVal(),
        "trnNote" : req.body.comment,
        "trnAmt" : req.body.amount,
        "currency" : "INR",
        "paymentType" : "P2P",
        "trnType" : "PAY",
        "mcccode" : "7399",
        "expiryTime" : "",
        "payeeAccNo" : "",
        "payeeIFSC" : "",
        "payeeAadharNo" : "",
        "payeeMobNo" : "",
        "payeeVirtualAdd" : req.body.vpa,
        "subMerchantID" : "",
        "whiteListAcc" : "",
        "payeeMmid" : "",
        "refURL" : "",
        "transferType" : "UPI",
        "payeeName" : req.body.userName,
        "payeeAddress" : "",
        "payeeEmail" : "",
        "payerAccNo" : "",
        "payerIFSC" : "",
        "payerMobNo" : "",
        "PAYYE VPA TYPE" : "VPA",
        "addField1" : "",
        "addField2" : "",
        "addField3" : "",
        "addField4" : "",
        "addField5" : "",
        "addField6" : "",
        "addField7" : "",
        "addField8" : "",
        "addField9" : "NA",
        "addField10" : "NA"
    };
    var yesbankReqArray = [], yesbankRequestString = "";

    for (var k in reqObj) {
        if (Object.prototype.hasOwnProperty.call(reqObj, k)) {
            yesbankReqArray.push(reqObj[k]);
        }
    }
    yesbankRequestString = yesbankReqArray.join("|")
    console.log(yesbankRequestString, '===================================')
    upiSecurity.encrypt(yesbankRequestString, "P2M",function (err, data) {
        console.log(data, '======================')

        var options = {
            method: 'POST',
            url: config.API.ME_PAY_SERVER_REQUEST,
            headers: {
                'X-IBM-Client-ID': config.CLIENT_ID,
                'X-IBM-Client-Secret': config.SECRET_ID,
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                requestMsg: data,
                pgMerchantId: config.p2mSpecialMerchantId
            }),
            agent: agent
        };

        request(options, function (error, response, body) {
            console.log("\n -------Error-------\n");
            console.log(error)
            console.log("\n -------response-------\n");
            console.log(response)
            console.log("\n -------body-------\n");
            console.log(body)
            if (error) {
                res.status(400).send({"statusCode": 400, "data": err})
            }
            else {
                upiSecurity.decrypt(body, 'P2M', function (err, data) {
                    console.log(err, data)
                    var resp = data.split("|")
                    res.status(200).send({"statusCode": 200, "data": resp})
                })
            }
        });


    })
}

/********************PROVIDE ENCRYPTION DATA */
function encryptData(req, res, next) {
    const requestMsg = req.body.requestMsg;
    const merchantKey = req.body.merchantKey;
    if (requestMsg === null) {
        res.status(200).send({"msg": "Please provide request msg "});
    }
    if (merchantKey === null) {
        res.status(200).send({"msg": "Please provide valid merchant key "});
    }
    var encryptedMessage = upiSecurity.encrypt(requestMsg, merchantKey);
    res.status(200).send({"requestMsg": encryptedMessage, "pgMerchantId": merchantKey});
    return;
}
